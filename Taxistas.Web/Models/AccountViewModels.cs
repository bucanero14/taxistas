﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Taxistas.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        public RegisterViewModel()
        {
            Roles = new List<SelectListItem>();
            SelectedRoles = new List<string>();
        }

        public string Id { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "Teléfono")]
        [StringLength(10, ErrorMessage = "El teléfono debe tener 10 caracteres.", MinimumLength = 10)]
        public string Telefono { get; set; }

        [Display(Name = "Roles")]
        public IList<SelectListItem> Roles { get; set; }

        public IList<string> SelectedRoles { get; set; }
    }
}
