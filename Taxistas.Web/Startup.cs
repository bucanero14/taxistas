﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Taxistas.Web.Startup))]

namespace Taxistas.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
