﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Taxistas.Core.Entities;
using Taxistas.Services.RoleService;
using Taxistas.Web.Helpers;
using Taxistas.Web.Models;

namespace Taxistas.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminUsuariosController : BaseController
    {
        private readonly IRoleService _roleService;

        public AdminUsuariosController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        // GET: AdminUsuarios
        public ActionResult Index()
        {
            var users = UserManager.Users.ToList();

            return View(users);
        }

        // GET: AdminUsuarios/Create
        public ActionResult Agregar()
        {
            ViewBag.Title = "Agregar Usuario";

            RegisterViewModel model = new RegisterViewModel
            {
                Roles = new List<SelectListItem>()
            };

            var roles = _roleService.GetRoles();

            roles.ToList().ForEach(x => model.Roles.Add(new SelectListItem { Text = x.Descripcion, Value = x.Name }));

            return View("Form", model);
        }

        // POST: AdminUsuarios/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Agregar(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Nombre = model.Nombre, PhoneNumber = model.Telefono };
                var result = await UserManager.CreateAsync(user, "Taxistas01");

                if (result.Succeeded)
                {
                    result = await UserManager.AddToRolesAsync(user.Id, model.SelectedRoles.ToArray());
                    return RedirectToAction("Index", "AdminUsuarios");
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Form", model);
        }

        public async Task<ActionResult> Editar(string id)
        {
            var user = await UserManager.FindByIdAsync(id);

            var roles = _roleService.GetRoles();

            RegisterViewModel model = new RegisterViewModel
            {
                Email = user.Email,
                Nombre = user.Nombre,
                Telefono = user.PhoneNumber,
                Roles = new List<SelectListItem>(),
                SelectedRoles = await UserManager.GetRolesAsync(user.Id)
            };

            roles.ToList().ForEach(x => model.Roles.Add(new SelectListItem { Text = x.Descripcion, Value = x.Name }));

            ViewBag.Title = "Editar Usuario";
            return View("Form", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Editar(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(model.Id);

                user.Nombre = model.Nombre;
                user.PhoneNumber = model.Telefono;
                user.Email = model.Email;

                var result = await UserManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    result = await UserManager.RemoveFromRolesAsync(user.Id, _roleService.GetRoles().Select(x=>x.Name).ToArray());

                    result = await UserManager.AddToRolesAsync(user.Id, model.SelectedRoles.ToArray());

                    return RedirectToAction("Index", "AdminUsuarios");
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Form", model);
        }

        [HttpPost]
        public JsonResult Desactivar(string id)
        {
            var result = UserManager.SetLockoutEnabledAsync(id, true);

            if (result.Result.Succeeded)
            {
                result = UserManager.SetLockoutEndDateAsync(id, DateTimeOffset.UtcNow.AddDays(365 * 100));
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public JsonResult Restablecer(string id)
        {


            string password = PasswordGenerator.GeneratePassword(true, true, true, true, false, 8);

            while(!PasswordGenerator.PasswordIsValid(true, true, true, true, false, password))
                password = PasswordGenerator.GeneratePassword(true, true, true, true, false, 8);

            var task = UserManager.RemovePasswordAsync(id);

            if (task.Result.Succeeded)
            {
                task = UserManager.AddPasswordAsync(id, password);
                return Json(new { password = password });
            }

            return Json(new { password = "" });
        }

        [HttpPost]
        public JsonResult Reactivar(string id)
        {
            var result = UserManager.SetLockoutEnabledAsync(id, false);

            if (result.Result.Succeeded)
            {
                result = UserManager.ResetAccessFailedCountAsync(id);
                return Json(true);
            }

            return Json(false);
        }
    }
}
