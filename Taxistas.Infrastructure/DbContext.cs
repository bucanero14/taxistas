﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Taxistas.Core;
using Taxistas.Core.Entities;


namespace Taxistas.Infrastructure
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class TaxistasDbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        public TaxistasDbContext()
           : base("TaxistasContext", throwIfV1Schema: false)
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
}
