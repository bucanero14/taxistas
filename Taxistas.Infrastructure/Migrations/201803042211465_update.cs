namespace Taxistas.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "FechaCreacion", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Blogs", "FechaActualizacion", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Blogs", "ActualizadoPor_Id", c => c.String(maxLength: 128, storeType: "nvarchar"));
            AddColumn("dbo.Blogs", "CreadoPor_Id", c => c.String(maxLength: 128, storeType: "nvarchar"));
            CreateIndex("dbo.Blogs", "ActualizadoPor_Id");
            CreateIndex("dbo.Blogs", "CreadoPor_Id");
            AddForeignKey("dbo.Blogs", "ActualizadoPor_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Blogs", "CreadoPor_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Blogs", "CreadoPor_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Blogs", "ActualizadoPor_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Blogs", new[] { "CreadoPor_Id" });
            DropIndex("dbo.Blogs", new[] { "ActualizadoPor_Id" });
            DropColumn("dbo.Blogs", "CreadoPor_Id");
            DropColumn("dbo.Blogs", "ActualizadoPor_Id");
            DropColumn("dbo.Blogs", "FechaActualizacion");
            DropColumn("dbo.Blogs", "FechaCreacion");
        }
    }
}
