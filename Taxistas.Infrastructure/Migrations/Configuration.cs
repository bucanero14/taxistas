using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using System.Linq;
using Taxistas.Core.Entities;

namespace Taxistas.Infrastructure.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<TaxistasDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;            
        }

        protected override void Seed(TaxistasDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Roles.AddOrUpdate(
                p => p.Name,
                new Role { Name = "Admin", Descripcion = "Administrador" }
            );
            
            if (!context.Users.Any(u=>u.Email == "bucanero14@gmail.com"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "bucanero14@gmail.com", Email = "bucanero14@gmail.com", Nombre = "Ra�l Maga�a", PhoneNumber = "9999685637" };

                manager.Create(user, "Nintendo01");
                manager.AddToRole(user.Id, "Admin");
            }
        }
    }
}
