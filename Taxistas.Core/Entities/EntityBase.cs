﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxistas.Core.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime FechaActualizacion { get; set; }

        public virtual ApplicationUser CreadoPor { get; set; }

        public virtual ApplicationUser ActualizadoPor { get; set; }
    }
}
