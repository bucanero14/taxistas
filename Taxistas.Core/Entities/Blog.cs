﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxistas.Core.Entities
{
    public class Blog : EntityBase
    {
        public string Name { get; set; }
        public IList<Post> Posts { get; set; }
    }
}
