﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Taxistas.Core.Entities
{
    public class Role : IdentityRole
    {
        public string Descripcion { get; set; }
    }
}
