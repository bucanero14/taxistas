﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxistas.Core.Interfaces
{
    public interface IIdentityRepository<T> : IDisposable where T : class
    {
        /// <summary>
        /// Gets All Entities with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        IQueryable<T> GetAllNoTracking { get; }
    }
}
