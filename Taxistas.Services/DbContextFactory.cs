﻿using Taxistas.Core;
using Taxistas.Infrastructure;

namespace Taxistas.Services
{
    public class DbContextFactory
    {
        public static IDbContext Create()
        {
            return new TaxistasDbContext();
        }
    }
}
