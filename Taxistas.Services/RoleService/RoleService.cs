﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taxistas.Core.Entities;
using Taxistas.Core.Interfaces;

namespace Taxistas.Services.RoleService
{
    public class RoleService : IRoleService
    {
        private readonly IIdentityRepository<Role> _roleRepository;

        public RoleService(IIdentityRepository<Role> roleRepository)
        {
            this._roleRepository = roleRepository;
        }

        public IEnumerable<Role> GetRoles()
        {
            return _roleRepository.GetAllNoTracking;
        }
    }
}
