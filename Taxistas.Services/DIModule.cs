﻿using Taxistas.Core.Entities;
using Taxistas.Core.Interfaces;
using Taxistas.Infrastructure.Repositories;
using Ninject.Modules;

namespace Taxistas.Services
{
    public class DIModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository<Blog>>().To<Repository<Blog>>();
            Bind<IIdentityRepository<Role>>().To<IdentityRepository<Role>>();
        }
    }
}
